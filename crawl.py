import csv
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from services.spider.get_casino_launcher import get_casino_url
from services.spider.get_element import get_element
from services.spider.get_game_launchers import get_game_launchers
from services.spider.get_game_launchers import get_game_launchers_URL
from services.spider.get_livecasino_launcher import get_livecasino_id
from services.spider.get_quicksilver_launcher import get_quicksilver_status
from services.spider.get_vegas_launcher import get_vegas_status
from services.spider.get_xsell_launcher import get_xsell_launcher
from services.spider.login import master_login


class GameSpider():
    """
        A class responsible for crawling the website.
        Initiates the driver, login, and checking
    """

    def __init__(self, config):
        self._config = config
        self.url_to_crawl = self._config.get_url_login()
        self.games_bad_status = []

    # Open headless chromedriver
    def start_driver(self):
        chrome_options = Options()
        headless = self._config.get_is_headless()

        if headless:
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--no-sandbox")
            chrome_options.add_argument("--disable-dev-shm-usage")
            chrome_options.add_argument("--window-size=360,740")
        chrome_options.add_argument(
            '--user-agent=Mozilla/5.0 (Linux; Android 10.0; SM-G965F Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Mobile Safari/537.36')
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-plugins")
        chrome_options.add_argument("--disable-hang-monitor")
        chrome_options.add_argument("--disable-mojo-renderer")
        chrome_options.add_argument("--disable-new-content-rendering-timeout")
        chrome_options.add_argument("--disable-partial-raster")
        chrome_options.add_argument("--disable-renderer-accessibility")
        chrome_options.add_argument("--disable-renderer-backgrounding")
        chrome_options.add_argument("--disable-gpu-appcontainer")
        chrome_options.add_argument("--disable-gpu-compositing")
        chrome_options.add_argument("--disable-gpu-driver-bug-workarounds")
        chrome_options.add_argument("--disable-gpu-early-init")
        chrome_options.add_argument("--disable-gpu-lpac")
        chrome_options.add_argument("--disable-gpu-memory-buffer-compositor-resources")
        chrome_options.add_argument("--disable-gpu-memory-buffer-video-frames")
        chrome_options.add_argument("--disable-gpu-process-crash-limit")
        chrome_options.add_argument("--disable-gpu-process-for-dx12-vulkan-info-collection")
        chrome_options.add_argument("--disable-gpu-program-cache")
        chrome_options.add_argument("--disable-gpu-rasterization")
        chrome_options.add_argument("--disable-gpu-sandbox")
        chrome_options.add_argument("--disable-gpu-shader-disk-cache")
        chrome_options.add_argument("--disable-gpu-vsync")
        chrome_options.add_argument("--disable-gpu-watchdog")
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)

    # Close chromedriver
    def close_driver(self):
        self.driver.quit()

    # Tell the browser to get a page
    def get_page(self, url):
        self.driver.get(url)

    def check_games_status(self, games=None):
        games_bad_status = []
        vertical = self._config.get_vertical()
        if vertical == 'casino':
            games_bad_status = get_casino_url(self.driver, self._config.get_sections())
        elif vertical == 'livecasino':
            games_bad_status = get_livecasino_id(self.driver, self._config, self._config.yaml.get('livecasion_api_url'))
        elif vertical == 'vegas':
            games_bad_status = get_vegas_status(self.driver, self._config.get_sections(),
                                                self._config.yaml.get('vegas_api_urls'))
        elif vertical == 'quicksilver':
            games_bad_status = get_quicksilver_status(self.driver, self._config.get_sections(),
                                                      self._config.yaml.get('quicksilver_data'))
        elif vertical == 'xsell':
            games_bad_status = get_xsell_launcher(self.driver, self._config.get_sections(),
                                                  self._config.yaml.get('xsell_data'))
        else:
            games_bad_status = get_game_launchers_URL(self.driver, games, self._config)
        return games_bad_status

    def start(self):
        self.start_driver()
        self.get_page(self.url_to_crawl)

        try:
            print("\nAttempting to click the cookie banner")
            get_element(self.driver, 'cookie-disclaimer__button', By.CLASS_NAME, 10).click()
        except Exception as e:
            print(f"Probably cookie banner cookie is set. Exception: {e}")

        master_login( self.driver, self._config)
        games = []
        if (self._config.get_vertical() in ('games', 'casino')):
            games = get_game_launchers(self.driver, self._config.get_sections())

        self.games_bad_status = self.check_games_status(games)
        self.close_driver()

    def get_bad_status_games(self):
        return self.games_bad_status
