from time import sleep

from config import GameCheckerConfig
from crawl import GameSpider

from services.raise_pd_incident import RaisePDIncident

def start():

    game_checker_config = GameCheckerConfig()

    vertical = game_checker_config.get_vertical()

    game_spider = GameSpider(game_checker_config)
    game_spider.start()
    bad_status_games = game_spider.get_bad_status_games()
 ### check if the list of games to alert with isn't empty and whether alerting is enabled in .env
    if bad_status_games and game_checker_config.get_is_alerting():
        pd_inc = RaisePDIncident(vertical=vertical, bad_status_games=bad_status_games)
        pd_inc.trigger_pd_incident()

if __name__ == "__main__":
    start()
