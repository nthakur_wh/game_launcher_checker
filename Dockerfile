FROM python:3.8-slim-buster

LABEL maintainer="svc_gaming_aws@williamhill.co.uk"

USER root

# install dependencies for Google Chrome
RUN apt-get update && apt-get install -y \
    fonts-liberation libappindicator3-1 libasound2 libatk-bridge2.0-0 \
    libnspr4 libnss3 libgbm1 libxss1 \
    xdg-utils wget

RUN CHROME_SETUP=google-chrome.deb && \
    wget -O $CHROME_SETUP "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb" && \
    dpkg -i $CHROME_SETUP && \
    apt-get install -y -f && \
    rm $CHROME_SETUP

RUN mkdir /app
WORKDIR /app

COPY requirements.txt start.sh ./
RUN pip install pip --upgrade
RUN pip install -r requirements.txt

COPY . .

RUN chmod +x ./start.sh
CMD ./start.sh