from services.util import get_env
from services.yaml import YAML


class GameCheckerConfig:
    def __init__(self):
        # Env vars
        self.is_headless = get_env("HEADLESS") == "True"
        self.vertical = get_env("VERTICAL") if get_env("VERTICAL") else "games"
        self.is_alerting = get_env("ALERTING") == "True"
        print("vertical is.....", self.vertical)
        print("Headless is .....", self.is_headless)
        print("Alerting is .....", self.is_alerting)
        yaml = YAML(f'config-{self.vertical}.yml')
        self.yaml = yaml
        # Yaml config
        self.sections = yaml.get("sections")

        # a dictionary of login parameters
        login_dict = self.__nested_yaml_key_to_dict("login")

        self.url_base = self.yaml.get("base_url")
        self.url_login = self.url_base + login_dict["url"]

        self.username = login_dict["user"]
        self.password = login_dict["password"]

    def __nested_yaml_key_to_dict(self, key):
        base_value = self.yaml.get(key)
        internal_dict = {}
        for element in base_value:
            internal_dict.update(element)
        return internal_dict

    def get_is_headless(self):
        return self.is_headless

    def get_vertical(self):
        return self.vertical

    def get_is_alerting(self):
        return self.is_alerting

    def get_url_base(self):
        return self.url_base

    def get_url_login(self):
        return self.url_login

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def get_sections(self):
        return self.sections
