from requests import post
from json import dumps
ROUTING_KEY = "1d24be64ad5c4f4da73a819efb5bfa77"  # ENTER EVENTS V2 API INTEGRATION KEY HERE
class RaisePDIncident:
    def __init__(self, bad_status_games, vertical="games", ROUTING_KEY="1d24be64ad5c4f4da73a819efb5bfa77"):
        self.vertical = vertical
        self.bad_status_games = bad_status_games
        self.ROUTING_KEY = ROUTING_KEY
    def get_payload(self):
        """
            Builds & returns a payload sent to PagerDuty to raise an incident
            Returns: String
        """
        summary = f"Some games on {self.vertical} might not be launchable"
        payload = {  # Payload is built with the least amount of fields required to trigger an incident
            "routing_key": self.ROUTING_KEY,
            "event_action": "trigger",
            "payload": {
                "summary": summary,
                "source": "Gaming_SRE:GameChecker",
                "severity": "info",
                "custom_details": {
                    "vertical": self.vertical,
                    "games": self.bad_status_games,
                }
            }
        }
        return payload
    def trigger_pd_incident(self):
        """ Triggers a PagerDuty incident without a previously generated incident key
         Uses Events V2 API - documentation: https://v2.developer.pagerduty.com/docs/send-an-event-events-api-v2
         Prints success or error message.
         """
        header = {
            "Content-Type": "application/json"
        }
        payload = self.get_payload()
        response = post('https://events.pagerduty.com/v2/enqueue', data=dumps(payload), headers=header)
        if response.json()["status"] == "success":
            print(
                'Incident created with with dedup key (also known as incident / alert key) of ' + '"' + response.json()[
                    'dedup_key'] + '"')
        else:
            print(response.text)  # print error message if not successful