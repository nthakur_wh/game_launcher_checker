import yaml

class YAML:
    def __init__(self, yaml_file):
        with open(yaml_file) as stream:
            try:
                self.yaml = yaml.safe_load(stream)
            except yaml.YAMLError as e:
                print(e)
    def get(self, key):
        return self.yaml[key]