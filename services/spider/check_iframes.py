from selenium.webdriver.common.by import By
from .get_element import get_element
from services.spider.get_game_launchers import close_game
from time import sleep
from .output import output
from .strategy.strategy1 import strategy1
from .strategy.strategy2 import strategy2

def check_iframes(driver, games, output_file):
    o = output(output_file)    
    bad_status_games = []
    for game in games:
        try:
            game_url_check = check_iframe(driver, game, o)
            if game_url_check["result"] is not False:
                bad_status_games.append(game_url_check) 
        except Exception as e:
            switch_to_default(driver)
            print(f"\n\nException: {e}")
            print("\n")
        print("bad_status_games :",bad_status_games)
    return bad_status_games 

#### False means that the game is ok
def check_iframe(driver, game, o):
    sleep(2)
    game_launcher = game.get_attribute('href')
    game.click()
    iframe = get_iframe(driver)
    result=False
    if iframe is None:
        result = "iFrameIsNone"
    else:
        sleep(3)
        driver.switch_to.frame(iframe)
        
        strategies = [strategy1, strategy2]
        for strategy in strategies:
            result = True
            if strategy(driver):
                result = False
                break
        sleep(1)
        switch_to_default(driver)
        close_game(driver)

    write_game_to_file(game_launcher, result, o)
    return {"game_url":game_launcher, "result":result}
    
def switch_to_default(driver):
    driver.switch_to.default_content()

def get_iframe(driver):
    sleep(1)
    try:
        iframe = get_element(driver, 'whgg-game-launcher__iframe-content', By.CLASS_NAME, 3)
        if iframe:
            return iframe
        else:
            return None
    except:
        return None

def write_game_to_file(game_launcher, result, o):
    o.write(game_launcher, result)