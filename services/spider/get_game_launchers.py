from time import sleep

from .strategy.find_canvas import find_canvas


# import services.spider.check_iframes


def get_by_attributes(driver, elem_name, attr, value):
    return driver.find_elements_by_class_name(value)


def get_game_launchers(driver, sections):
    games = []
    section_elements = [
        {'elem': get_by_attributes(driver, section['name'], section['attr'], section['value']), **section} for section
        in sections]
    for section in section_elements:
        if 'header-name' in section:
            for s in section['elem']:
                header = get_by_attributes(s, section['header-name'], section['header-attr'],
                                           section['header-spec-value'])
                if len(header) > 0:
                    games += find_game_tile(s)
        else:
            games += find_game_tile(section['elem'][0])
    return games


def close_game(driver):
    driver.find_element_by_class_name('whggc-icon__x').click()
    sleep(1)


def find_game_tile(section):
    games = []
    game_launch_urls = set()
    for a in section.find_elements_by_tag_name('a'):
        try:
            if 'action/launch' in a.get_attribute('href'):
                game_launch_urls.add(a.get_attribute('href'))
                games.append(a)
        except Exception:
            pass
    return games


def get_game_launchers_URL(driver, games, _config):
    games_status = []
    for url in [game.get_attribute('href') for game in games]:
        if "mega-ball" not in url:
            game_status_check = find_canvas(url, driver)
            if game_status_check["result"] is not True:
                games_status.append(game_status_check)
    return games_status
