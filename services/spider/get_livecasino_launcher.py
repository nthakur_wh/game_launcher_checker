from json import loads

from jsonpath_ng import parse
from requests import post

from .strategy.find_canvas import find_canvas

headers = {'Content-Type': 'application/json'}
get_livecasino_games_id_payload = '''{
  sections(where: { id_in: ["live-casino-exclusive-tables"] }, vertical: LIVE_CASINO) {
    id
    ... on GameTileSection {
      allGames {
        totalCount
        edges {
          node {
         id

          }
        }
      }
    }
  }
}'''

get_livecsino_active_game_status_payload = '''{
  allGames(
    where: {
      origin: [GCC]
      id: { in: ["::get_livecasion_id"] }
    }
  ) {
    edges {
      node {
        id
        code
        title
        table {
          overlay {
            title
          }
        }
      }
    }
  }

}'''


def get_livecasino_id(driver, section, livecasion_api_url):
    games_status = []
    livecasino_status = []
    gaming_api_url_livecasino = livecasion_api_url['gaming_api_url_livecasino']
    response = post(gaming_api_url_livecasino, headers=headers,
                    json={"operationName": None, "variables": {}, "query": get_livecasino_games_id_payload})
    json_string = response.text
    json_data = loads(json_string)
    jsonpath_expression = parse('data.sections..id')
    match = jsonpath_expression.find(json_data)
    for match in jsonpath_expression.find(json_data):
        activegames_response = fetch_livecasino_activegames(match.value, gaming_api_url_livecasino)
        livecasinourl = "https://livecasino.williamhill.com/action/launch/" + match.value
        availability_check = "available" in activegames_response
        availability_check1 = "\"table\":null" in activegames_response
        if availability_check or availability_check1:
            print(f"Game is not available for {livecasinourl}")
        else:
            if (livecasinourl.find('gcc::') > 0):
                livecasino_status = find_canvas(livecasinourl, driver)
                if livecasino_status["result"] is not True:
                    games_status.append(livecasino_status)
    return games_status


def fetch_livecasino_activegames(livecasinoid, gaming_api_url_livecasino):
    datalivecasino = get_livecsino_active_game_status_payload.replace('::get_livecasion_id', livecasinoid)
    headers = {'Content-Type': 'application/json'}
    response = post(gaming_api_url_livecasino, headers=headers,
                    json={"operationName": None, "variables": {}, "query": datalivecasino})
    return response.text
