from time import sleep

from .strategy1 import strategy1


def strategy2(driver):
    iframes = get_iframe(driver)
    for iframe in iframes:
        is_down = strategy2(iframe)
        if is_down:
            driver.switch_to.frame(iframe)
            return strategy1(iframe)
        else:
            return True
    return False

def get_iframe(driver):
    try:
        sleep(1)
        return driver.find_elements_by_tag_name("iframe")
    except:
        return []