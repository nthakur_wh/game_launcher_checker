from datetime import datetime

from .strategy1 import strategy1
from .strategy2 import strategy2


def find_canvas(url, driver):
    game_exception = ""
    result = False
    try:
        driver.get(url)
        strategies = [strategy1, strategy2]
        for strategy in strategies:
            result = False
            if strategy(driver):
                result = True
                break
        if not result:
            raise Exception("No canvas element found")

    except Exception as e:
        game_exception = e
    current_dt = datetime.now()
    datetime_game = current_dt.strftime("%d-%m-%Y %H:%M:%S")

    current_game_result_dict = {"time": datetime_game, "game_url": url, "result": result,
                                "exception": str(game_exception)}
    print(current_game_result_dict)
    return current_game_result_dict
