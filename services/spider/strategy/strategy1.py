from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

def strategy1(driver):
    canvas = None
    try:
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.TAG_NAME, "canvas")))
        canvas = driver.find_elements_by_tag_name('canvas')
    except Exception as e:
        pass
    
    if canvas is not None and len(canvas) > 0:
        return True
    else:
        return False