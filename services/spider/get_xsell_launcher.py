import time

from selenium.webdriver.common.by import By

from .strategy.find_canvas import find_canvas
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

def get_xsell_launcher(driver, sections, xsell_data):
    games_status = []
    xsell_url = []
    if (driver.find_elements_by_xpath(xsell_data['xsell_landing_page_xpath']) is not None):
        driver.get(xsell_data['xsell_login_url'])
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, "//*[@class='iframe-quicksilver']")))
        driver.switch_to.frame(driver.find_element_by_xpath("//*[@class='iframe-quicksilver']"))
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, xsell_data['xpath_jackpot'])))
        for xsell_xpath in sections:
            if (xsell_xpath['sectiontext'] in ( 'Roulette and Blackjack',  'Scratchcards',)):
                xpath_with_hyperlink =  xsell_data['xpath_with_hyperlink'].replace('::replacetext', xsell_xpath['sectiontext'])
                xsell_status_url = driver.find_elements_by_xpath(xpath_with_hyperlink)
            else:
                xpath_without_hyperlink = xsell_data['xpath_without_hyperlink'].replace('::replacetext', xsell_xpath['sectiontext'])
                xsell_status_url = driver.find_elements_by_xpath(xpath_without_hyperlink)
            for append_url in [game.get_attribute('href') for game in xsell_status_url]:
                xsell_url.append(append_url)
        status = [find_canvas(url, driver) for url in set(xsell_url)]
        games_status.extend([stat for stat in status if stat["result"] is not True])

    else:
        print("Exiting.. As page is landed to quicksilver page")
    return games_status
