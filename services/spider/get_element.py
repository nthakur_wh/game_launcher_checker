from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def get_element(driver, selector, By, timeout = 1):
    return WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By, selector)))
