from .strategy.find_canvas import find_canvas


def get_by_attributes(driver, elem_name, attr, value):
    return driver.find_elements_by_class_name(value)


def get_casino_url(driver, sections):
    url = []
    games_status = []
    casino_section_url = []
    xpath = "((//*[contains(text(),'"
    xpath1 = "')])[2]/../following-sibling::div[1]/div/div[1]/div[1]/div/a)"
    for casino_xpath in sections:
        casino_status_url = driver.find_elements_by_xpath(xpath + casino_xpath['sectiontext'] + xpath1)

        temp_list = [game.get_attribute('href') for game in casino_status_url]
        for appendurl in temp_list:
            casino_section_url.append(appendurl)

    for url in set(casino_section_url):
        result = url.find('action/launch')
        if int(result) > 0:
            casino_status = find_canvas(url, driver)
            if casino_status["result"] is not True:
                games_status.append(casino_status)
    return games_status
