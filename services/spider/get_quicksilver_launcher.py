from json import loads

from requests import request

from .login import quicksilver_xsell_login
from .strategy.find_canvas import find_canvas


def get_quicksilver_status(driver, sections, quicksilver_data):
    games_status = []
    qs_api_url = quicksilver_data['qs_api_url']
    driver.get(quicksilver_data['QS_login_url'])
    qs_payload = quicksilver_data['qs_api_postdata']
    headers = {'Content-Type': 'application/json'}
    response = request("POST", qs_api_url, headers=headers, data=qs_payload)
    launch_urls = loads(response.text.encode('utf8'))
    for casino_xpath in sections:
        section = list(filter(lambda game: game['node']['title'].lower() == casino_xpath['sectiontext'].lower(),
                              launch_urls['data']['allCategories']['edges']))
        launch_urls1 = [game['node']['launchUrl'] for game in section[0]['node']['games']['edges']]
        for qsurl in launch_urls1:
            vegas_status = find_canvas(qsurl, driver)
            if vegas_status["result"] is not True:
                games_status.append(vegas_status)

    return games_status
