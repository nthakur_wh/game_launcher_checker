from requests import request
from json import loads
from .strategy.find_canvas import find_canvas


def get_game_categories(driver, sections, gamecategory_api_url):
    response = request("GET", gamecategory_api_url)
    json_string = response.text
    json_data = loads(json_string)
    games_id = []
    a = []
    for game in json_data:
        for casino_xpath in sections:
            game_list = list(filter(lambda x: x['label'] == casino_xpath['sectiontext'], game['list']))
            if len(game_list) > 0:
                for i in range(0, 15):
                    games_id.append(game_list[0]['games'][i])
    return games_id


def get_vegas_status(driver, sections, vegas_api_url):
    gamecategory_api_url = vegas_api_url['gamecategory_api_url']
    gamesection_api_url = vegas_api_url['gamesection_api_url']
    vegas_baseurl = vegas_api_url['vegas_baseurl']
    games_status = []
    response1 = request("GET", gamesection_api_url)
    data = response1.json()
    for x in set(get_game_categories(driver, sections, gamecategory_api_url)):
        vegas_status = find_canvas((vegas_baseurl + data[x]['code']), driver)
        if vegas_status["result"] is not True:
            games_status.append(vegas_status)
    return games_status
