from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from .get_element import get_element


def login(driver, username, password):
    try:
        timeout = 10
        get_element(driver, 'username', By.NAME, timeout).send_keys(username)
        get_element(driver, 'password', By.NAME, timeout).send_keys(password)
        get_element(driver, 'login-submit-button', By.NAME, timeout).click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.TAG_NAME, "title")))
    except Exception as e:
        print("some error", e)
        pass

def quicksilver_xsell_login(driver, username, password):
    try:
        timeout = 10
        get_element(driver, 'username', By.ID, timeout).send_keys(username)
        get_element(driver, 'password', By.ID, timeout).send_keys(password)
        get_element(driver, 'login-button', By.ID, timeout).click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.TAG_NAME, "title")))
    except Exception as e:
        print("some error", e)
        pass

def master_login(driver, config):
    username=config.get_username()
    password=config.get_password()
    if(config.get_vertical() in ('quicksilver','xsell')):
        quicksilver_xsell_login(driver, username, password)
    else:
        login(driver, username, password)

