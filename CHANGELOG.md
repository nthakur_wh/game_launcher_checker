# [0.8.0](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/compare/0.7.4...0.8.0) (2020-08-07)


### New

* [GSRE-327] remove CSV creation functionality and Pager Duty alerting ([3d8dbb2](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/commit/3d8dbb2a8d79f0248ce67343864cc991b4528ebf))

## [0.7.4](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/compare/0.7.3...0.7.4) (2020-08-07)


### Fix

* skip the release ([8959e73](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/commit/8959e737a2324f193825818b143c561a18267b61))

## [0.7.3](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/compare/0.7.2...0.7.3) (2020-08-06)


### Fix

* use master branch ([9428bcd](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/commit/9428bcd26444e6372137e8784ba0187a63c63910))

## [0.7.2](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/compare/0.7.1...0.7.2) (2020-08-06)


### Fix

* reorder the stages ([8da9f40](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/commit/8da9f40d1fd2229d4e20fb9f787c77af33463b14))

## [0.7.1](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/compare/0.7.0...0.7.1) (2020-08-06)


### Fix

* try a test version of the repo ([9a46f4e](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/commit/9a46f4ea07c4d50d4f82c9761bf103da56a6bdc7))

# [0.7.0](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/compare/0.6.2...0.7.0) (2020-08-06)


### New

* [GSRE-317] create a pipeline for building a docker image; minor cleanup ([919b373](https://git.nonprod.williamhill.plc/gaming/sre/game_launcher_checker/commit/919b37388ac61f854fcb0874ed094c42362abbc0))
