# Game Launch Checker

- [Game Launch Checker](#game-launch-checker)
  - [Overview](#overview)
  - [GitLab CI](#gitlab-ci)
    - [Automatic CHANGELOG](#automatic-changelog)
    - [Docker image](#docker-image)
  - [Local install & run](#local-install--run)
    - [Install on host](#install-on-host)
    - [Run on host](#run-on-host)
    - [Install a Docker image](#install-a-docker-image)
    - [Run via Docker Compose](#run-via-docker-compose)
    - [Use Nexus](#use-nexus)
  - [Server (VM) install & run](#server-vm-install--run)
    - [Build an image](#build-an-image)
    - [Set up a run script & logging](#set-up-a-run-script--logging)
    - [Set up crontab](#set-up-crontab)
  - [Links](#links)

## Overview

The purpose of the application is to verify whether the games in the chosen vertical are lanuchable.

---

## GitLab CI

This project uses GitLab CI for two things:

[1) to generate a CHANGELOG](#automatic-changelog)

[2) to build a Docker image and push it to Nexus](#docker-image)

### Automatic CHANGELOG

The `CHANGELOG.md` file is generated/updated by [the GitLab CI Automatic Changelog Tool](https://git.nonprod.williamhill.plc/gaming/cloud/tools/gitlab-ci-automatic-changelog).
For the tool to work properly [you must follow its commit naming convention](https://git.nonprod.williamhill.plc/gaming/cloud/tools/gitlab-ci-automatic-changelog#caveats),
e.g.: `New: add something`, `Fix: make something work properly`.

### Docker image

The `./Dockerfile` is built and pushed to [Nexus](https://nexus.dtc.prod.williamhill.plc:8443) by [the docker-build tool](https://git.nonprod.williamhill.plc/gaming/cloud/tools/docker-build)
every time a Git tag is generated.

---

## Local install & run

### Install on host

Prerequisites:

- Python v3.7+

To launch the application locally, not in Docker one needs to have a local python environment, preferrably a venv. And example of an installation could be:

```bash
cd PATH_TO_THE_GAME_CHECKER
python3 -m venv game_checker_env
source game_checker_env/bin/activate
pip3 install -r requirements.txt
```

### Run on host

```bash
./start.sh
```

### Install a Docker image

Prerequisites:

- Docker CE v18+

Download the repo, and do:

```bash
cd PATH_TO_THE_GAME_CHECKER
docker build -t game_checker ./
```

### Run via Docker Compose

Simplest version (defaults to run on games or whatever is setup as VERTICAL in .env)

```bash
docker-compose up
```

Running on a different vertical requires setting an env variable to the initial part of the URL, e.g. casino.williamhill.com => casino:

```bash
VERTICAL=casino docker-compose up
```

### Use Nexus

You can also pull an image down from [our Docker image repository hosted at Nexus](https://nexus.dtc.prod.williamhill.plc:8443) and then run it:

```bash
docker login docker-registry.prod.williamhill.plc
docker pull docker-registry.prod.williamhill.plc/gaming/game_launcher_checker:<TAG> # swap <TAG> for an existing version tag or branch name, e.g. 1.0.0, gsre-317
docker run docker-registry.prod.williamhill.plc/gaming/game_launcher_checker:<TAG>
```

---

## Server (VM) install & run

Current setup allows for running via crontab, but not multiple verticals at the same time, as those will try to launch the same container, just with different variables.

Expected directory structure:

```text
game_launch_checker
│   start_game_launch_checker.sh
│
└───game_launcher_checker_master
    │   docker-compose.yml
    │   ... (project files)
```

In the desired directory run the following commands to set up the structure:

```bash
mkdir game_launch_checker
cd game_launch_checker
mkdir log
vim start_game_launch_checker.sh #script contents can be found below
```

Project files need to be delivered as possible. Possibly by downloading this project to the local machine, and using SCP to get them to the machine, and build there, e.g.:

```bash
scp game_launcher_checker-master.zip ec2-user@my-server-ip:/home/ec2-user/game_launch_checker
unzip game_launcher_checker-master.zip
```

Alternatively an image could be built locally, and sent to the remote machine.

### Build an image

```bash
cd ... /game_launch_checker/game_launcher_checker_master
docker build -t game_checker ./
```

_NB_ Alternatively, you can [pull down an existing image from Nexus](#use-nexus)

### Set up a run script & logging

The script should be created in a directory above the docker-compose.yml or the commands and configuration variables should be amended accordingly.

```bash
#!/bin/bash

# The main purpose of the script is to provide an easy setup for running Game Launch Checker.
# Requirements:
#     - log directory is available under the location described in the logdir (default - `mkdir log` at the location of the script)
#     - user running the script has to be able to run docker-compose

# ${0%/*} used below is the directory where the script is located.
# Hence logs and paths are in relation to where the script is, independently on the current workdir.
# The main reason is that cron uses a different workdir, and it was found to be the simplest solution, independent on the user.

docker_compose_path="${0%/*}"'/game_launcher_checker-master/docker-compose.yml'
logdir="${0%/*}"/log
gaming_vertical=$1
if [ -z "$1" ]
then
  gaming_vertical="games"
fi

# Log start info
echo "`date` INFO: RUN START" | tee -a $logdir/"$gaming_vertical".run.log $logdir/"$gaming_vertical".error.log 1>/dev/null

# Docker compose up
VERTICAL=$gaming_vertical WH_ENV=. HEADLESS=True ALERTING=True docker-compose -f $docker_compose_path up 1>>$logdir/"$gaming_vertical".run.log  2>>$logdir/"$gaming_vertical".error.log

# Log end info
echo "`date` INFO: RUN END" | tee -a $logdir/"$gaming_vertical".run.log $logdir/"$gaming_vertical".error.log 1>/dev/null

# Docker compose down
VERTICAL=$gaming_vertical WH_ENV=. HEADLESS=True ALERTING=True docker-compose -f $docker_compose_path dow 1>>$logdir/"$gaming_vertical".run.log  2>>$logdir/"$gaming_vertical".error.log
```

### Set up crontab

An example of the setup can be seen below:

```text
5 1,3,5,7,9,11,13,15,17,19,21,23 * * * /home/ec2-user/game_launch_checker/start_game_launch_checker.sh games
25 1,3,5,7,9,11,13,15,17,19,21,23 * * * /home/ec2-user/game_launch_checker/start_game_launch_checker.sh casino
1 0,2,4,6,8,10,12,14,16,18,20,22 * * * /home/ec2-user/game_launch_checker/start_game_launch_checker.sh livecasino
20 0,2,4,6,8,10,12,14,16,18,20,22 * * * /home/ec2-user/game_launch_checker/start_game_launch_checker.sh vegas
40 0,2,4,6,8,10,12,14,16,18,20,22 * * * /home/ec2-user/game_launch_checker/start_game_launch_checker.sh quicksilver
```

The differences in hours and minutes of runs should provide enough time in-between, to not overlap, therefore allowing runs to finish. This can be verified through logs (RUN START - RUN END), e.g.:

```bash
cd game_launch_checker/log

```

## Links

- [Confluence documentation](https://conf.willhillatlas.com/display/GAMINGTEAM/Game+launcher+checker)
